# Hurl gitlab ci sample

This is a sample project with few examples on how to use hurl in gitlab ci/cd.

Please read the [.gitlab-ci.yml](.gitlab-ci.yml) configuration file and check pipelines to view the results.
